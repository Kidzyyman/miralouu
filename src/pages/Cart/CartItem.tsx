import { FC } from 'react'

import { useCartSummary } from '@/components/hooks/useCartSummary'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'
import { fixedNumTenth } from '@/utils/fixNumTenth'

const CartItem: FC<IProductsCarouselData | any> = ({
	id,
	image,
	title,
	subtitle,
	price,
	cartQuantity,
	cartItem
}) => {
	const { handleDecreaseCart, handleIncreaseCart, handleRemoveFromCart } =
		useCartSummary()

	return (
		<div className='cart-item' key={id}>
			<div className='cart-product'>
				<img src={image} alt={title} />
				<div>
					<div className='cart-product-block'>
						<h4 className='cart-product__title'>{title}</h4>
						<p className='cart-product__subtitle'>{subtitle}</p>
						<button onClick={() => handleRemoveFromCart(cartItem)}>
							Remove
						</button>
					</div>
				</div>
			</div>

			<div className='cart-product-price'>${price}</div>
			<div className='cart-product-quantity'>
				<button onClick={() => handleDecreaseCart(cartItem)}>-</button>
				<div className='count'>{cartQuantity}</div>
				<button onClick={() => handleIncreaseCart(cartItem)}>+</button>
			</div>
			<div className='cart-product-total-price'>
				${fixedNumTenth(price * cartQuantity)}
			</div>
		</div>
	)
}

export default CartItem

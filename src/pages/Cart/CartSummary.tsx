import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import { useCartSummary } from '@/components/hooks/useCartSummary'
import { getTotals } from '@/store/cart/cart'
import { ICartSummary } from '@/types/TPages/ICart/ICartSummary'
import { fixedNumTenth } from '@/utils/fixNumTenth'

import CartItem from './CartItem'

const CartSummary: FC<ICartSummary> = ({ cart }) => {
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(getTotals())
	}, [cart, dispatch])

	const { handleClearCart } = useCartSummary()

	return (
		<div>
			<div className='titles'>
				<h3 className='product-title'>Product</h3>
				<h3 className='price'>Price</h3>
				<h3 className='quantity'>Quantity</h3>
				<h3 className='total'>Total</h3>
			</div>
			<div className='cart-items'>
				{cart.cartItems?.map(cartItem => (
					<CartItem {...cartItem} cartItem={cartItem} />
				))}
			</div>
			<div className='cart-summary'>
				<button onClick={() => handleClearCart()} className='clear-cart'>
					Clear Cart
				</button>
				<div className='cart-checkout'>
					<div className='subtotal'>
						<span>Subtotal</span>
						<span className='amount'>
							${fixedNumTenth(cart.cartTotalAmount)}
						</span>
					</div>
					<p>Taxes and shipping calculated at checkout</p>
					<button>Check out</button>
					<div className='continue-shopping'>
						<Link to='/home'>
							<svg
								xmlns='http://www.w3.org/2000/svg'
								width='20'
								height='20'
								fill='currentColor'
								className='bi bi-arrow-left'
								viewBox='0 0 16 16'
							>
								<path
									fillRule='evenodd'
									d='M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z'
								/>
							</svg>
							<span>Continue Shopping</span>
						</Link>
					</div>
				</div>
			</div>
		</div>
	)
}
export default CartSummary

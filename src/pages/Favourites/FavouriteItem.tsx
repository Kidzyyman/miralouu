import { FC } from 'react'

import FavoriteIcon from '@/components/ProductsCarousel/FavouriteIcon'
import { IFavouriteItem } from '@/types/IFavourite/IFavouriteItem'

const FavouriteItem: FC<IFavouriteItem> = ({
	item,
	handleRemoveFavourites
}) => {
	return (
		<div key={item.id} className='favourites-summary-block'>
			<img
				src={item.image}
				alt={item.title}
				className='favourites-summary-block__img'
			/>
			<div className='favourites-summary-block__descrBlock'>
				<FavoriteIcon
					favouriteItem={item}
					addFavouriteItem={() => handleRemoveFavourites(item)}
				/>
				<h2 className='favourites-summary-block__title'>{item.title}</h2>
				<h3 className='favourites-summary-block__subtitle'>{item.subtitle}</h3>
				<h3 className='favourites-summary-block__price'>${item.price}</h3>
			</div>
		</div>
	)
}

export default FavouriteItem

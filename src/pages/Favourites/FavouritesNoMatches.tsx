import Lottie from 'react-lottie'

import useWindowDimensions from '@/utils/headerResize'

import noMatchesGrass from '../../assets/noMatchesInput-grass.json'

const FavouritesNoMatches = () => {
	const { width } = useWindowDimensions()
	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: noMatchesGrass,
		renderSettings: {
			preserveAspectRatio: 'xMidYMid slice'
		}
	}

	return (
		<section className='favourites-noMatches'>
			<Lottie
				options={defaultOptions}
				height={250}
				width={width < 569 ? width - 400 + 300 : 400}
			/>
			<p className='favourites-noMatches__descr'>Hmm..no matches found</p>
		</section>
	)
}

export default FavouritesNoMatches

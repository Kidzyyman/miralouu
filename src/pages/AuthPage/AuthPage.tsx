import { FC } from 'react'

import AuthForm from './AuthForm/AuthForm'

const AuthPage: FC = () => {
	return (
		<div className='auth-page'>
			<AuthForm />
		</div>
	)
}
export default AuthPage

import React from 'react'
import {
	Controller,
	SubmitHandler,
	useForm,
	useFormState
} from 'react-hook-form'
import { useNavigate } from 'react-router-dom'

import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'

import { loginValidation, passwordValidation } from './validation'

interface ISignInForm {
	login: string
	password: string
}

const AuthForm = () => {
	const { handleSubmit, control } = useForm<ISignInForm>()
	const { errors } = useFormState({
		control
	})

	const navigate = useNavigate()
	const onSubmit: SubmitHandler<ISignInForm> = () => {
		//logic backend...
		navigate('/home', { replace: true })
	}
	return (
		<div className='auth-form'>
			<Typography variant='h4' component='div' gutterBottom={true}>
				Log in
			</Typography>
			<Typography
				variant='subtitle1'
				component='div'
				gutterBottom={true}
				className='auth-form__subtitle'
			>
				to get access
			</Typography>
			<form className='auth-form__form' onSubmit={handleSubmit(onSubmit)}>
				<Controller
					control={control}
					name='login'
					rules={loginValidation}
					render={({ field }) => (
						<TextField
							label='Username'
							size='small'
							margin='normal'
							className='auth-form__input'
							fullWidth={true}
							onChange={e => field.onChange(e)}
							value={field.value}
							error={!!errors.login?.message}
							helperText={errors.login?.message}
						/>
					)}
				/>
				<Controller
					control={control}
					name='password'
					rules={passwordValidation}
					render={({ field }) => (
						<TextField
							label='Password'
							size='small'
							margin='normal'
							className='auth-form__input'
							fullWidth={true}
							onChange={e => field.onChange(e)}
							value={field.value}
							error={!!errors.password?.message}
							helperText={errors.password?.message}
						/>
					)}
				/>

				<Button
					type='submit'
					variant='contained'
					fullWidth={true}
					disableElevation={true}
					color='success'
					sx={{
						marginTop: 2
					}}
				>
					Sign in
				</Button>
			</form>
		</div>
	)
}
export default AuthForm

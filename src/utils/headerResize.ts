import { useEffect, useState } from 'react'

import { IWindowDimensions } from '@/types/TUtils/IWindowDimensions'

const getWindowDimensions = (): IWindowDimensions => {
	const { innerWidth: width, innerHeight: height } = window
	return {
		width,
		height
	}
}

export default function useWindowDimensions(): IWindowDimensions {
	const [windowDimensions, setWindowDimensions] = useState<IWindowDimensions>(
		getWindowDimensions()
	)

	useEffect(() => {
		function handleResize(): void {
			setWindowDimensions(getWindowDimensions())
		}

		window.addEventListener('resize', handleResize)
		return (): void => window.removeEventListener('resize', handleResize)
	}, [])

	return windowDimensions
}

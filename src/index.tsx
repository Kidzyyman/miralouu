import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'

import { StyledEngineProvider } from '@mui/material/styles'

import App from './App'
import Toastify from './components/ui/toastify/Toastify'
import { store } from './store'
import ScrollToTop from './utils/ScrollToTop'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
	<Provider store={store}>
		<React.StrictMode>
			<BrowserRouter>
				<StyledEngineProvider injectFirst>
					<ScrollToTop />
					<Toastify breakpoint={769} />
					<App />
				</StyledEngineProvider>
			</BrowserRouter>
		</React.StrictMode>
	</Provider>
)

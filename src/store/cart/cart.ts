import { toast } from 'react-toastify'

import { createSlice } from '@reduxjs/toolkit'

import { ICartState } from '@/types/TStore/ICartState'

const initialState: ICartState = {
	cartTotalQuantity: 0,
	cartTotalAmount: 0,
	cartItems: localStorage.getItem('cartItems')
		? JSON.parse(localStorage.getItem('cartItems') || '')
		: []
}

export const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		addToCart(state, action) {
			const itemIndex = state.cartItems.findIndex(item => {
				return item.id === action.payload.id
			})

			if (itemIndex >= 0) {
				state.cartItems[itemIndex].cartQuantity += 1
				toast.info(
					`increased ${state.cartItems[itemIndex].title} cart quantity`,
					{
						position: 'bottom-left',
						autoClose: 1500
					}
				)
			} else {
				const tempProduct = { ...action.payload, cartQuantity: 1 }
				state.cartItems.push(tempProduct)

				toast.success(`${action.payload.title} added to cart`, {
					position: 'bottom-left',
					theme: 'dark',
					autoClose: 1500
				})
			}

			localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
		},
		removeFromCart(state, action) {
			const nextCartItems = state.cartItems.filter(
				cartItem => cartItem.id !== action.payload.id
			)
			if (nextCartItems.length === 0) {
				state.cartTotalQuantity = 0
			}
			state.cartItems = nextCartItems
			localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
			toast.error(`${action.payload.title} removed from cart`, {
				position: 'bottom-left',
				theme: 'dark',
				autoClose: 1500
			})
		},
		decreaseCart(state, action) {
			const itemIndex = state.cartItems.findIndex(
				cartItem => cartItem.id === action.payload.id
			)

			if (state.cartItems[itemIndex].cartQuantity > 1) {
				state.cartItems[itemIndex].cartQuantity -= 1
				toast.info(`Decreased${action.payload.title} cart quantity`, {
					position: 'bottom-left',
					theme: 'dark',
					autoClose: 1500
				})
			} else if (state.cartItems[itemIndex].cartQuantity === 1) {
				const nextCartItems = state.cartItems.filter(
					cartItem => cartItem.id !== action.payload.id
				)

				state.cartItems = nextCartItems

				toast.error(`${action.payload.title} removed from cart`, {
					position: 'bottom-left',
					theme: 'dark',
					autoClose: 1500
				})
			}
			localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
		},
		clearCart(state) {
			state.cartItems = []
			state.cartTotalQuantity = 0
			toast.error(`Cart cleared`, {
				position: 'bottom-left',
				theme: 'dark',
				autoClose: 1500
			})
			localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
		},
		getTotals(state) {
			let { total, quantity } = state.cartItems.reduce(
				(cartTotal, cartItem) => {
					const { price, cartQuantity } = cartItem
					const itemTotal = price * cartQuantity

					cartTotal.total += itemTotal
					cartTotal.quantity += cartQuantity

					return cartTotal
				},
				{ total: 0, quantity: 0 }
			)
			state.cartTotalQuantity = quantity
			state.cartTotalAmount = total
		}
	}
})

export const { addToCart, removeFromCart, decreaseCart, clearCart, getTotals } =
	cartSlice.actions
export default cartSlice.reducer

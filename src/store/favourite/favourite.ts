import { createSlice } from '@reduxjs/toolkit'

import { IFavouriteState } from '@/types/TStore/IFavouriteState'

import {
	fetchAddFavourite,
	fetchAddFavouriteService,
	fetchFavourites
} from './services/FavouritesApi'

const initialState: IFavouriteState = {
	products: [],
	status: null,
	error: null,
	loading: false
}

export const favouriteSlice = createSlice({
	name: 'favourite',
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder
			.addCase(fetchFavourites.pending, state => {
				state.loading = true
				state.status = 'loading'
				state.error = null
			})
			.addCase(fetchFavourites.fulfilled, (state, action) => {
				state.products = action.payload
				state.status = 'resolved'
				state.loading = false
			})
			.addCase(fetchFavourites.rejected, (state, action) => {
				state.status = 'rejected'
				state.error = action.payload
			})
			.addCase(fetchAddFavourite.pending, state => {
				state.loading = true
				state.status = 'loading'
				state.error = null
			})
			.addCase(fetchAddFavourite.fulfilled, (state, action) => {
				fetchAddFavouriteService(state, action)
				state.status = 'resolved'
				state.loading = false
			})
	}
})

export default favouriteSlice.reducer

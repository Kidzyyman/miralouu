import { FC } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import BiographyDetail from '@/components/PlantBiography/BiographyDetail/BiographyDetail'
import AboutUs from '@/pages/AboutUs/AboutUs'
import AuthPage from '@/pages/AuthPage/AuthPage'
import Cart from '@/pages/Cart/Cart'
import ContactUs from '@/pages/ContactUs/ContactUs'
import Favourites from '@/pages/Favourites/Favourites'
import Home from '@/pages/Home/Home'
import News from '@/pages/News/News'
import Products from '@/pages/Products/Products'

const AppRoutes: FC = () => (
	<Routes>
		<Route path='/signin' element={<AuthPage />} />
		<Route path='/home' element={<Home />} />
		<Route path='/aboutUs' element={<AboutUs />} />
		<Route path='/Products' element={<Products />} />
		<Route path='/News' element={<News />} />
		<Route path='/contactUs' element={<ContactUs />} />
		<Route path='/cart' element={<Cart />} />
		<Route path='/biography/:id' element={<BiographyDetail />} />
		<Route path='*' element={<Navigate to='/signin' replace />} />
		<Route path='/favourites' element={<Favourites />} />
	</Routes>
)
export default AppRoutes

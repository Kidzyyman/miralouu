import { FC } from 'react'

import { motion } from 'framer-motion'

import { framerMotion } from '@/utils/FramerMotion'

import { MPlantBioStatisticDetails } from './PlantBioStatisticDetails'
import { plantBioStatisticData } from './mockStatistic'

const PlantBioStatistic: FC = () => {
	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='plantBioStatistic-section'
		>
			<div className='container-second'>
				<div className='plantBioStatistic-wrapper'>
					{plantBioStatisticData.map((item, i) => (
						<MPlantBioStatisticDetails
							custom={i + 1}
							variants={framerMotion.plantGridDataAnimation}
							key={item.id}
							{...item}
						/>
					))}
				</div>
			</div>
		</motion.section>
	)
}

export default PlantBioStatistic

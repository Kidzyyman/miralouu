export interface IStatistic {
	id: string
	image: string
	total: number
	title: string
}

export const plantBioStatisticData: IStatistic[] = [
	{
		id: '1',
		image: require('../../assets/icons/plantBio-icons/plantBio-user.svg')
			.default,
		total: 2000,
		title: 'Total Service'
	},
	{
		id: '2',
		image: require('../../assets/icons/plantBio-icons/plantBio-delivery.svg')
			.default,
		total: 1000,
		title: 'Home Delivary'
	},
	{
		id: '3',
		image: require('../../assets/icons/plantBio-icons/plantBio-users.svg')
			.default,
		total: 5000,
		title: 'Happy Client'
	},
	{
		id: '4',
		image: require('../../assets/icons/plantBio-icons/plantBio-experts.svg')
			.default,
		total: 3000,
		title: 'Total Experts'
	}
]

import { FC, ForwardedRef, forwardRef } from 'react'

import { motion } from 'framer-motion'

import { IStatistic } from './mockStatistic'

const PlantBioStatisticDetails: FC<IStatistic> = forwardRef(
	({ image, total, title }, ref: ForwardedRef<HTMLDivElement>) => {
		return (
			<div ref={ref} className='plantBioStatistic-block'>
				<img
					src={image}
					className='plantBioStatistic-block__icon'
					alt={title}
				/>
				<div className='plantBioStatistic-block__total'>{total}</div>
				<div className='plantBioStatistic-block__descr'>{title}</div>
			</div>
		)
	}
)

export default PlantBioStatisticDetails

export const MPlantBioStatisticDetails = motion(PlantBioStatisticDetails)

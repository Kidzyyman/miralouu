import { useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { AnyAction, Dispatch } from '@reduxjs/toolkit'
import { SwiperRef } from 'swiper/react'

import { useTypedSelector } from '@/pages/Cart/Cart'
import { addToCart } from '@/store/cart/cart'
import {
	fetchAddFavourite,
	fetchFavourites
} from '@/store/favourite/services/FavouritesApi'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'
import useWindowDimensions from '@/utils/headerResize'

export const useProducts = () => {
	const swiperRef = useRef<SwiperRef>(null)
	const { width } = useWindowDimensions()
	const dispatch = useDispatch<Dispatch<AnyAction>>()
	const navigate = useNavigate()
	const { products, status } = useTypedSelector(state => state.favouriteReducer)

	const addToCartItems = (product: IProductsCarouselData) => {
		dispatch(addToCart(product))
	}

	const addToFavouriteItems = (product: IProductsCarouselData) => {
		dispatch(fetchAddFavourite(product))
	}

	useEffect(() => {
		dispatch(fetchFavourites())
	}, [dispatch])

	return {
		swiperRef,
		dispatch,
		navigate,
		products,
		status,
		addToCartItems,
		addToFavouriteItems,
		width
	}
}

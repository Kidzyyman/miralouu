import React, { useEffect, useState } from 'react'

import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

export const useSearchInput = () => {
	const [searchInput, setSearchInput] = useState<string>('')
	const [searchParams] = useState<string[]>(['title'])
	const [matches, setMatches] = useState<boolean>()
	const searchMatches: boolean[] = []

	const searchFavourites = (
		e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	): void => {
		setSearchInput(e.currentTarget.value)
	}

	useEffect(() => {
		setMatches(searchMatches.includes(true))
	}, [searchInput])

	const handleSearch = (favourite: IProductsCarouselData[]) =>
		favourite.filter((item: any) => {
			const search = searchParams.some(
				(newItem: string) =>
					item[newItem]
						.toString()
						.toLowerCase()
						.indexOf(searchInput.toLowerCase()) > -1
			)
			searchMatches.push(search)
			return search
		})

	return {
		searchFavourites,
		handleSearch,
		searchInput,
		searchParams,
		matches
	}
}

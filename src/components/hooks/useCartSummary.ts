import { useDispatch } from 'react-redux'

import {
	addToCart,
	clearCart,
	decreaseCart,
	removeFromCart
} from '@/store/cart/cart'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

import { useTypedSelector } from '../Header/CartIcon'

export const useCartSummary = () => {
	const dispatch = useDispatch()

	const { cartTotalQuantity, cartItems } = useTypedSelector(
		state => state.cartReducer
	)

	const handleRemoveFromCart = (cartItem: IProductsCarouselData) => {
		dispatch(removeFromCart(cartItem))
	}
	const handleDecreaseCart = (cartItem: IProductsCarouselData) => {
		dispatch(decreaseCart(cartItem))
	}
	const handleIncreaseCart = (cartItem: IProductsCarouselData) => {
		dispatch(addToCart(cartItem))
	}
	const handleClearCart = () => {
		dispatch(clearCart())
	}

	return {
		dispatch,
		handleRemoveFromCart,
		handleDecreaseCart,
		handleIncreaseCart,
		handleClearCart,
		cartItems,
		cartTotalQuantity
	}
}

import { FC } from 'react'

import UserReviewsCarousel from './UserReviewsCarousel'
import UserReviewsTitle from './UserReviewsTitle'

const UserReviews: FC = () => {
	return (
		<>
			<UserReviewsTitle />
			<UserReviewsCarousel />
		</>
	)
}

export default UserReviews

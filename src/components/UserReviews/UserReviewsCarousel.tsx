import { FC, useEffect, useState } from 'react'

import axios from 'axios'
import { motion } from 'framer-motion'
import { Autoplay, Pagination } from 'swiper'
import 'swiper/css'
import 'swiper/css/effect-coverflow'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

import { IUserReviewsCarousel } from '@/types/IUserReviews/IUserReviews'
import { framerMotion } from '@/utils/FramerMotion'

import { useProducts } from '../hooks/useProducts'
import { MUserReviewsItem } from './UserReviewItem'

const UserReviewsCarousel: FC = () => {
	const [usersList, setUsersList] = useState<IUserReviewsCarousel[]>([])
	const { swiperRef, width } = useProducts()

	useEffect(() => {
		const data = axios.get('https://63c325198bb1ca34755e00d7.mockapi.io/users')
		data.then(responce => setUsersList(responce.data))
	}, [])

	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='userReviews-section'
			onMouseEnter={() => swiperRef.current?.swiper.autoplay.stop()}
			onMouseLeave={() => swiperRef.current?.swiper.autoplay.start()}
		>
			<div className='container-second'>
				<Swiper
					autoplay={
						width > 998 ? { delay: 4000, disableOnInteraction: false } : false
					}
					breakpoints={{
						1600: {
							slidesPerView: 3
						},
						1100: {
							slidesPerView: 2
						},

						769: {
							slidesPerView: 1
						}
					}}
					slidesPerView={1}
					spaceBetween={30}
					pagination={{
						clickable: true
					}}
					modules={[Pagination, Autoplay]}
					className='mySwiper'
					ref={swiperRef}
				>
					<div className='userReviewsCarousel-wrapper'>
						{usersList.map((item: IUserReviewsCarousel, i) => (
							<SwiperSlide key={item.id}>
								<MUserReviewsItem
									custom={i + 1}
									variants={framerMotion.animationRightToLeft}
									{...item}
								/>
							</SwiperSlide>
						))}
					</div>
				</Swiper>
			</div>
		</motion.section>
	)
}

export default UserReviewsCarousel

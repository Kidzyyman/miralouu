import { FC } from 'react'

const MainButtonWhite: FC = () => {
	return (
		<button className='main-btn__white'>
			<span>Learn More</span>
		</button>
	)
}
export default MainButtonWhite

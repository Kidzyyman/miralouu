import { FC } from 'react'

import { motion } from 'framer-motion'

import { framerMotion } from '@/utils/FramerMotion'

import ProductsCarousel from '../ProductsCarousel/ProductsCarousel'
import MainButtonGreen from './MainButtonGreen'
import MainButtonWhite from './MainButtonWhite'
import { MMainButton } from './MainButtons'
import { MMainImage } from './MainImage'

const Main: FC = () => {
	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='section-main'
		>
			<div className='container'>
				<div className='main-block'>
					<div className='main-block__left'>
						<motion.h2
							custom={1}
							variants={framerMotion.textAnimation}
							className='main-block__descr'
						>
							#Ornamental Plant
						</motion.h2>
						<motion.h1
							custom={2}
							variants={framerMotion.textAnimation}
							className='main-block__title'
						>
							Various Indoor Plant Shop
						</motion.h1>
						<motion.h3
							custom={3}
							variants={framerMotion.textAnimation}
							className='main-block__subtitle'
						>
							Lorem Ipsum is simply dummy text of the printing and typesetting
							industry. Lorem Ipsum has been.
						</motion.h3>

						<MMainButton custom={4} variants={framerMotion.textAnimation}>
							<MainButtonGreen />
							<MainButtonWhite />
						</MMainButton>
					</div>

					<div className='main-block__right'>
						<MMainImage custom={2} variants={framerMotion.imageAnimation} />
					</div>
				</div>
			</div>
			<ProductsCarousel />
		</motion.section>
	)
}
export default Main

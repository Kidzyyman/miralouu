import { FC } from 'react'

import { motion } from 'framer-motion'

import { Props } from '@/types/TProps'

export const sectionAnimation = {
	hidden: {
		x: -300,
		y: 100,
		opacity: 0
	},
	visible: (custom: number) => ({
		x: 0,
		y: 0,
		opacity: 1,
		transition: { delay: custom * 0.3 }
	})
}

const PlantCollection: FC<Props> = ({ children }) => {
	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='plantCollection-section'
		>
			<div className='container'>{children}</div>
			<button className='main-btn__empty_middle'>View All</button>
		</motion.section>
	)
}
export default PlantCollection

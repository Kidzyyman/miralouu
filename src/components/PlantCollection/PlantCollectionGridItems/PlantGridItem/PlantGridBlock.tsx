import { FC, ForwardedRef, forwardRef } from 'react'

import { motion } from 'framer-motion'

import { IPlantCollection } from '@/types/IPlantCollection/IPlantCollection'

const PlantGridItem: FC<IPlantCollection> = forwardRef(
	({ img, quantity, title }, ref: ForwardedRef<HTMLDivElement>) => {
		return (
			<div ref={ref} className='plantGridItem'>
				<img className='plantGridItem__img' src={img} alt={img}></img>
				<div className='plantGridItem-info'>
					<h2 className='plantGridItem__title'>{title}</h2>
					<div className='plantGridItem-quantity'>
						<span className='plantGridItem-quantity__counter'>{quantity}</span>
						<h2 className='plantGridItem-quantity__title'>items</h2>
					</div>
				</div>
			</div>
		)
	}
)
export default PlantGridItem
export const MPlantGridItem = motion(PlantGridItem)

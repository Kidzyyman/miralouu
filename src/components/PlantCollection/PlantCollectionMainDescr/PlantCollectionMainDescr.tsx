import { FC, ForwardedRef, forwardRef } from 'react'

import { motion } from 'framer-motion'

const PlantCollectionMainDescr: FC = forwardRef<HTMLDivElement>(
	(_, ref: ForwardedRef<HTMLDivElement>) => {
		return (
			<div ref={ref} className='plantCollection-main-descr'>
				<h2 className='plantCollection-main-descr__title'>Plant Collection</h2>
				<h2 className='plantCollection-main-descr__subtitle'>
					Smoothlu And healthy plant fore you and all people
				</h2>
			</div>
		)
	}
)
export default PlantCollectionMainDescr
export const MPlantCollectionMainDescr = motion(PlantCollectionMainDescr)

import { FC } from 'react'

import { IProductsCarousel } from '@/types/IProductCarousel/IProductCarousel'

import RatingItem from './RatingItem'

const FeaturedProductsItem: FC<IProductsCarousel> = ({ item, addToCart }) => {
	return (
		<div className='featuredProductsItem-item'>
			<div className='featuredProductsItem-item__block1'>
				<img
					src={item.image}
					alt={item.title}
					className='products-item__block1__img'
				/>
			</div>
			<div className='products-item__block2'>
				<div className='products-item__block2-descr'>
					<h2 className='products-item__block2__title'>{item.title}</h2>
					<button
						onClick={addToCart}
						className={
							'products-item-btn_block products-item-btn_block_skew products-item-btn_block_border'
						}
					>
						Add to Cart
					</button>
				</div>
				<div className='products-item__block2-descr2'>
					<div className='products-item__block2__price'>${item.price}</div>
					<RatingItem />
				</div>
			</div>
		</div>
	)
}
export default FeaturedProductsItem

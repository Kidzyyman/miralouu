import { FC, ForwardedRef, forwardRef } from 'react'
import { Link } from 'react-router-dom'

import { motion } from 'framer-motion'

import { IPlantBiographyItem } from '@/types/IPlantBiography/IPlantBiographyItem'

const PlantBiographyItem: FC<IPlantBiographyItem> = forwardRef(
	({ image, i, currentSlide }, ref: ForwardedRef<HTMLDivElement>) => {
		return (
			<div ref={ref} className='plantBiography-block'>
				<img className='plantBiography-block__img' src={image} alt={image} />
				{currentSlide === i && (
					<Link to={`/biography/${+currentSlide + 1}`}>
						<button className='plantBiography-btn'>Biography</button>
					</Link>
				)}
			</div>
		)
	}
)
export default PlantBiographyItem

export const MPlantBiographyItem = motion(PlantBiographyItem)

import { FC } from 'react'

import PlantBiographyCarousel from './PlantBiographyCarousel'
import PlantBiographyTitle from './PlantBiographyTitle'

const PlantBiography: FC = () => {
	return (
		<section className='plantBiography-section'>
			<div className='container-second'>
				<PlantBiographyTitle />
				<PlantBiographyCarousel />
			</div>
		</section>
	)
}

export default PlantBiography

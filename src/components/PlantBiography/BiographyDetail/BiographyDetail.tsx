import { FC } from 'react'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import Header from '@/components/Header/Header'
import BackLinkHome from '@/components/ui/buttons/BackLinkHome'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

import { getBio } from './services'

const BiographyDetail: FC = () => {
	const { id } = useParams<string>()
	const [biography, setBiography] = useState<IProductsCarouselData[] | any>([])

	useEffect(() => {
		getBio(id, setBiography)
	}, [id])

	return (
		<section className='biographyDetail'>
			<Header />
			<div className='biographyDetail-section'>
				<div className='container-second'>
					<div className='biographyDetail-section-main'>
						<img
							src={biography.image}
							alt={biography.title}
							className='biographyDetail-section__img'
							width={200}
							height={200}
						/>

						<div className='biographyDetail-section__rightSide'>
							<h1 className='biographyDetail-section__rightSide__title'>
								{biography.title}
							</h1>

							<h3 className='biographyDetail-section__rightSide__descr'>
								{biography.bio}
							</h3>
							<div className='btn_center'>
								<BackLinkHome />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default BiographyDetail

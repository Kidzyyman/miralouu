import { FC } from 'react'

import { mockPlantData } from './mockPlanData'

const PlantCare: FC = () => {
	return (
		<section className='plantCare-section'>
			<div className='container-second'>
				<div className='plantCollection-main-descr'>
					<h2 className='plantCollection-main-descr__title'>Plant Care</h2>
					<h2 className='plantCollection-main-descr__subtitle'>
						Smoothlu And healthy plant fore you and all people
					</h2>
				</div>
				<div className='plantCare-blocks'>
					{mockPlantData.map(item => (
						<div key={item.id} className='plantCare-block'>
							<div className='plantCare-block_circle'>
								<img
									src={item.image}
									alt={item.title}
									className='plantCare-block__img'
								/>
							</div>

							<div className='plantCare-block__descr'>
								<h3 className='plantCare-block__title'>{item.title}</h3>
								<h4 className='plantCare-block__subtitle'>
									{item.description}
								</h4>
							</div>
						</div>
					))}
				</div>
			</div>
		</section>
	)
}
export default PlantCare

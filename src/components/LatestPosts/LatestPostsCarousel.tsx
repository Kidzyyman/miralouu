import { FC } from 'react'

import { motion } from 'framer-motion'
import { Autoplay, Pagination } from 'swiper'
import 'swiper/css'
import 'swiper/css/effect-coverflow'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

import { ILatestPosts } from '@/types/ILatestPosts/ILatestPosts'
import { framerMotion } from '@/utils/FramerMotion'

import { useProducts } from '../hooks/useProducts'
import { MLatestPostsItem } from './LatestPostsItem'
import LatestPostsTitle from './LatestPostsTitle'
import { latestList } from './mockLatestPostData'

const LatestPostsCarousel: FC = () => {
	const { swiperRef, width } = useProducts()
	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='latestPost-section'
			onMouseEnter={() => swiperRef.current?.swiper.autoplay.start()}
			onMouseLeave={() => swiperRef.current?.swiper.autoplay.stop()}
		>
			<div className='container-second'>
				<LatestPostsTitle />
				<Swiper
					autoplay={
						width > 998 ? { delay: 4000, disableOnInteraction: false } : false
					}
					breakpoints={{
						1600: {
							slidesPerView: 4
						},
						1100: {
							slidesPerView: 3
						},
						769: {
							slidesPerView: 2
						}
					}}
					slidesPerView={1}
					spaceBetween={30}
					pagination={{
						clickable: true
					}}
					modules={[Pagination, Autoplay]}
					className='mySwiper'
					ref={swiperRef}
				>
					<div className='latestPost-wrapper'>
						{latestList.map((item: ILatestPosts, i) => (
							<SwiperSlide key={item.id}>
								<MLatestPostsItem
									custom={i + 1}
									variants={framerMotion.plantGridDataAnimation}
									{...item}
								/>
							</SwiperSlide>
						))}
					</div>
				</Swiper>
			</div>
		</motion.section>
	)
}

export default LatestPostsCarousel

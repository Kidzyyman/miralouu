import { FC } from 'react'
import { ToastContainer } from 'react-toastify'

import useWindowDimensions from '@/utils/headerResize'

interface IToastifyBreakpoint {
	breakpoint?: number
}

const Toastify: FC<IToastifyBreakpoint> = ({ breakpoint = '' }) => {
	const { width } = useWindowDimensions()
	return <>{width > breakpoint && <ToastContainer />}</>
}
export default Toastify

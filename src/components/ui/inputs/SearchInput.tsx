import { FC } from 'react'

import TextField from '@mui/material/TextField/TextField'

import { ISearchInput } from '@/types/IUI/ISearchInput'

const SearchInput: FC<ISearchInput> = ({ value, search }) => {
	return (
		<TextField
			style={{
				marginTop: '10px',
				borderRadius: '50px',
				maxWidth: '20rem',
				width: '100%'
			}}
			color='success'
			id='filled-basic'
			label='Search'
			variant='filled'
			value={value}
			placeholder='Search favourites product'
			onChange={e => search(e)}
		/>
	)
}

export default SearchInput

import { FC, useEffect, useState } from 'react'

import IconButton from '@material-ui/core/IconButton'
import { makeStyles } from '@material-ui/core/styles'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'

const useStyles = makeStyles(theme => ({
	toTop: {
		zIndex: 2,
		position: 'fixed',
		bottom: '6.9vh',
		backgroundColor: '#DCDCDC',
		color: 'black',
		'&:hover,&.Mui-focusVisible': {
			transition: '0.3s',
			color: '#393BA6',
			backgroundColor: 'DCDCDC'
		},
		left: '2%'
	}
}))

const GoToUpScroll: FC<any> = ({ showBelow }) => {
	const [show, setShow] = useState(showBelow ? false : true)

	const classes = useStyles()
	const handleScroll = () => {
		if (window.pageYOffset > showBelow) {
			if (!show) setShow(true)
		} else {
			if (show) setShow(false)
		}
	}

	useEffect(() => {
		if (showBelow) {
			window.addEventListener('scroll', handleScroll)
			return () => window.removeEventListener('scroll', handleScroll)
		}
	})

	const handleClick = () => {
		window['scrollTo']({ top: 0, behavior: 'smooth' })
	}
	return (
		<div>
			{show && (
				<IconButton onClick={handleClick} className={classes.toTop}>
					<ExpandLessIcon />
				</IconButton>
			)}
		</div>
	)
}
export default GoToUpScroll

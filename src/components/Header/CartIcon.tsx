import { FC, useEffect } from 'react'
import { TypedUseSelectorHook, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import Badge, { BadgeProps } from '@mui/material/Badge'
import IconButton from '@mui/material/IconButton'
import { styled } from '@mui/material/styles'

import { RootState } from '@/store'
import { getTotals } from '@/store/cart/cart'

import { useCartSummary } from '../hooks/useCartSummary'

const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
	'& .MuiBadge-badge': {
		right: -3,
		top: 13,
		border: `2px solid ${theme.palette.background.paper}`,
		padding: '0 4px'
	}
}))

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector

const CartIcon: FC = () => {
	const { cartItems, cartTotalQuantity, dispatch } = useCartSummary()

	useEffect(() => {
		dispatch(getTotals())
	}, [dispatch, cartItems])

	return (
		<div className='cart-icon'>
			<Link to={'/cart'}>
				<IconButton aria-label='cart'>
					<StyledBadge
						badgeContent={cartTotalQuantity}
						color='success'
						showZero
					>
						<ShoppingCartIcon />
					</StyledBadge>
				</IconButton>
			</Link>
		</div>
	)
}
export default CartIcon

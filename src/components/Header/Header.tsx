import { FC } from 'react'
import { Link } from 'react-router-dom'

import HeaderLogo from '@/assets/icons/header-icons/Logo.svg'
import useWindowDimensions from '@/utils/headerResize'

import HeaderDesktopItems from './HeaderDesktopItems'
import HeaderMobile from './HeaderMobile'

const Header: FC = () => {
	const { width } = useWindowDimensions()

	return (
		<section className='section-header'>
			<div className='container'>
				<nav className='nav'>
					<Link to={'/home'}>
						<img src={HeaderLogo} className='header-logo' alt={HeaderLogo} />
					</Link>
					{width > 992 ? <HeaderDesktopItems /> : <HeaderMobile />}
				</nav>
			</div>
		</section>
	)
}

export default Header

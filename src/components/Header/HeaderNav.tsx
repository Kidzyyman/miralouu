import { FC } from 'react'
import { Link } from 'react-router-dom'

const HeaderNav: FC = () => {
	const headerNavList: string[] = [
		'Home',
		'AboutUs',
		'Products',
		'News',
		'ContactUs'
	]

	return (
		<ul className='header-nav'>
			{headerNavList.map((item, i) => (
				<li className='header-nav__li' key={i}>
					<Link to={'/' + item}>{i === 1 ? 'About Us' : item}</Link>
				</li>
			))}
		</ul>
	)
}

export default HeaderNav

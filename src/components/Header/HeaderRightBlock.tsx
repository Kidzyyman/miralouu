import { FC } from 'react'

import PersonOutlineIcon from '@mui/icons-material/PersonOutline'

import CartIcon from './CartIcon'

const HeaderRightBlock: FC = () => {
	return (
		<div className='header-right-block'>
			<CartIcon />
			<PersonOutlineIcon fontSize='large' />
		</div>
	)
}
export default HeaderRightBlock

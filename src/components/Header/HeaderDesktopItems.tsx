import { FC } from 'react'

import HeaderNav from './HeaderNav'
import HeaderRightBlock from './HeaderRightBlock'

const HeaderDesktopItems: FC = () => {
	return (
		<>
			<HeaderNav />
			<HeaderRightBlock />
		</>
	)
}
export default HeaderDesktopItems

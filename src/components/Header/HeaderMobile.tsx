import { FC, useState } from 'react'

import FavouritesMainIcon from '@/pages/Favourites/FavouritesMainIcon'

import BurgerMenu from './BurgerMenu'
import CartIcon from './CartIcon'
import HeaderNav from './HeaderNav'

const HeaderMobile: FC = () => {
	const [activeBurger, setActiveBurger] = useState(false)

	return (
		<>
			<CartIcon />
			<FavouritesMainIcon />
			<BurgerMenu
				activeBurgerMenu={activeBurger}
				setActiveBurgerMenu={setActiveBurger}
			/>
			{activeBurger && (
				<>
					<HeaderNav />
				</>
			)}
		</>
	)
}
export default HeaderMobile

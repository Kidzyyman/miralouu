import { FC, memo } from 'react'

import { Autoplay, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

import { useProducts } from '../hooks/useProducts'
import ProductsItem from './ProductsItem'

const ProductsCarousel: FC = () => {
	const { swiperRef, products, addToCartItems, addToFavouriteItems, width } =
		useProducts()

	return (
		<section className='ProductsCarousel-section'>
			<div className='container'>
				<div
					className='ProductCarousel-carousel'
					onMouseEnter={() => swiperRef.current?.swiper.autoplay.stop()}
					onMouseLeave={() => swiperRef.current?.swiper.autoplay.start()}
				>
					<Swiper
						modules={[Navigation, Autoplay]}
						spaceBetween={30}
						slidesPerView={1}
						autoplay={
							width > 998 ? { delay: 4000, disableOnInteraction: false } : false
						}
						breakpoints={{
							1600: {
								slidesPerView: 4
							},
							1250: {
								slidesPerView: 3
							},
							992: {
								slidesPerView: 2
							}
						}}
						navigation={{
							nextEl: '.swiper-button-next',
							prevEl: '.swiper-button-prev'
						}}
						ref={swiperRef}
					>
						<div className='products-wrapper'>
							{products.map((item: IProductsCarouselData) => (
								<SwiperSlide key={item.id}>
									<ProductsItem
										item={item}
										addToCart={() => addToCartItems(item)}
										addToFavourite={() => addToFavouriteItems(item)}
									/>
								</SwiperSlide>
							))}
						</div>
						<span className='swiper-button-next'></span>
					</Swiper>
				</div>
			</div>
		</section>
	)
}
export default memo(ProductsCarousel)

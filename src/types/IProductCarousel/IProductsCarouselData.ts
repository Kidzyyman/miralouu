export interface IProductsCarouselData {
	id: string
	bio?: string
	image: string
	title: string
	subtitle: string
	choosen: boolean
	price: number
	cartQuantity?: number | any
	status?: any
	error?: any
}

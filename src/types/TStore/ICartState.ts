import { IProductsCarouselData } from '../IProductCarousel/IProductsCarouselData'

export interface ICartState {
	cartTotalQuantity: number
	cartTotalAmount: number
	cartItems: IProductsCarouselData[]
}

import { IProductsCarouselData } from '../IProductCarousel/IProductsCarouselData'

export interface IFavouriteState {
	status?: null | string
	error?: null | string
	loading?: boolean
	products: IProductsCarouselData[]
}

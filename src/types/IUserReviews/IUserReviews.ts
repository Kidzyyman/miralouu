export interface IUserReviewsCarousel {
	id: string
	photo: string
	title: string
	subtitle: string
	description: string
}

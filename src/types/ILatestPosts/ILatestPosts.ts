export interface ILatestPosts {
	id: string
	image: string
	date: string
	description: string
}

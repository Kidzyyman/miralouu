import { IProductsCarouselData } from '../IProductCarousel/IProductsCarouselData'

export interface IFavouriteItem {
	item: IProductsCarouselData
	handleRemoveFavourites: (item: IProductsCarouselData) => void
}

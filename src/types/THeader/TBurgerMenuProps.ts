export type TBurgerMenuProps = {
	activeBurgerMenu: boolean
	setActiveBurgerMenu: (activeBurgerMenu: boolean) => void
}

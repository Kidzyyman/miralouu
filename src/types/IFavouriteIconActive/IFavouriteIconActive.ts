import { IProductsCarouselData } from '../IProductCarousel/IProductsCarouselData'

export interface IFavouriteIconActive {
	favouriteItem: IProductsCarouselData
	addFavouriteItem?: any | (() => void)
}

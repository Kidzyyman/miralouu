export interface IPlantTitles {
	title: string
	subtitle?: string
	img: string
}
